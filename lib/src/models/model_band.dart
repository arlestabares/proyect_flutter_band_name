class Band {
//El backend generara el id, de momento lo genero
  String id;
  String name; //nombre de la banda
  int votes; //cantidad de votos que tendra esa banda

//
  Band({this.id, this.name, this.votes});

//El factory Constructor no es mas que un constructor que recibe cierto
//tipo de argumentos, y regresa una nueva instancia de la clase en este caso de Band(),
//el cual tiene por nombre... obj... que tiene que ser de tipo Map<>, y le establezco los valores.
  factory Band.fromMap(Map<String, dynamic> obj) => Band(
      //defino cada una de las propiedas anteriores.


      //Valido que cada uno de los valores llegue o exista con el .containsKey()
      id:    obj.containsKey('id')    ? obj['id']    : 'no-id',
      name:  obj.containsKey('name')  ?  obj['name'] : 'no-name',
      votes: obj.containsKey('votes') ? obj['votes'] : 'np-votes');
}
