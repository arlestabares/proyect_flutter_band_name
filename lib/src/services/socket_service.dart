import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

//cuando inicie la aplicacion va ha estar en Connecting
enum ServerStatus { OnLine, OffLine, Connecting }

//Este ChangeNotifier  nos ayudara notificandole a provider cuando tiene
//que refrescar la IU o interfaz de usuario, o redibujar algun Widget en particular.
//o cuando quiera notificar a las personas que esten trabajando con este SocketServer.
class SocketService with ChangeNotifier {
  ServerStatus _serverStatus = ServerStatus.Connecting;
  IO.Socket _socket;

  ServerStatus get serverStatus => this._serverStatus;

//El socket escucha los eventos del server
  IO.Socket get socket => this._socket;
  Function get emit => this._socket.emit;

  SocketService() {
    this._initConfig();
  }

  void _initConfig() {
// Dart client
    this._socket = IO.io('http://localhost:3000', {
      'transports': ['websocket'],
      'autoConnect': true
    });

    this._socket.onConnect((_) {
      print('connect');
      this._serverStatus = ServerStatus.OnLine;
      notifyListeners();
    });

    this._socket.onDisconnect((_) {
      print('disconnect11111');
      this._serverStatus = ServerStatus.OffLine;
      notifyListeners();
    });

    // this._socket.onConnect((_) {
    //   print('connect');
    // this._serverStatus = ServerStatus.OnLine;
    //   notifyListeners();
    // });

    // this._socket.onDisconnect((_) {
    //   print('disconnect');
    //   this._serverStatus = ServerStatus.OffLine;
    //   notifyListeners();
    // });

    // this._socket.on('nuevo-mensaje', (payload) {
    //   print('nuevo-mensaje $payload');
    //   print('Escuchando mensaje: $payload');
    //   print('nombre:' + payload['nombre']);
    //   print('mensaje:' + payload['mensaje']);
    //   print('mensaje:' + payload['mensaje2']);

    //print(payload.containsKeys('mensaje2') ? payload['mensaje2'] : 'no hay mensaje');
    // });
  }
}
