import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:proyect_flutter_band_name/src/services/socket_service.dart';

class StatusPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final socketService = Provider.of<SocketService>(context);
    //socketService.emit();
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[Text('${socketService.serverStatus}')],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.message),
          onPressed: () {
            //emitimos un mensaje que escuchara el server y luego lo remitira a todos los clientes, en este
            //caso el navegador.
            socketService.emit('emitir-mensaje',
                {'nombre': 'Flutter', 
                'mensaje': 'Hola desde flutter'});
          }
      ),
    );
  }
}
