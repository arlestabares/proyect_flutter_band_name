import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';

import 'package:pie_chart/pie_chart.dart';
import 'package:proyect_flutter_band_name/src/models/model_band.dart';
import 'package:proyect_flutter_band_name/src/services/socket_service.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Band> bands = [
    // Band(id: '1', name: 'Metallica', votes: 5),
    // Band(id: '2', name: 'Queen', votes: 1),
    // Band(id: '3', name: 'Heroes del Silencio', votes: 2),
    // Band(id: '4', name: 'Bon Jovi', votes: 5),
  ];

  @override
  void initState() {
    final socketService = Provider.of<SocketService>(context, listen: false);
    //Aqui escucho el evento al que deseo vincularme , el cual esta definido
    //en el server  socket.js, para obtener las bandas del band.getBands()

    socketService.socket.on('active-bands', _handleActiveBands);

    super.initState();
  }

//Mando solo la referencia de este metodo como argumento al socketService de arriba
  _handleActiveBands(dynamic payload) {
    //print(payload);
    //Mapeo el payload para  poder obtener una lista y asi transformarlo
    this.bands = (payload as List).map((band) => Band.fromMap(band)).toList();

    setState(() {
      //para que se redibuje el widget completo cuando se reciba un active-bands
    });
  }

  //Llamamos al dispose, para evitar estar escuchando cuando ya no se necesita
  @override
  void dispose() {
    final socketService = Provider.of<SocketService>(context, listen: false);
    socketService.socket.off('active-bands');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //referencia al socketservis
    final socketService = Provider.of<SocketService>(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text(
          'BandNames',
          style: TextStyle(color: Colors.black87),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          Container(
              margin: EdgeInsets.only(right: 10),
              child: (socketService.serverStatus == ServerStatus.OnLine)
                  ? Icon(Icons.check_circle, color: Colors.blue[300])
                  : Icon(Icons.offline_bolt, color: Colors.red))
        ],
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _showGraph(),
          Expanded(
            child: ListView.builder(
                itemCount: bands.length,
                itemBuilder: (context, i) => _bandTile(bands[i])),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add), elevation: 1, onPressed: addNewBand),
    );
  }

  Widget _bandTile(Band band) {
    //Hago referencia al socketService.
    final socketService = Provider.of<SocketService>(context, listen: false);

    return Dismissible(
      key: Key(band.id),
      direction: DismissDirection.startToEnd,
      onDismissed: (_) => socketService.emit('delete-band', {'id': band.id}),
      background: Container(
        color: Colors.red,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'Delete Band',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
      child: ListTile(
        leading: CircleAvatar(
          //Obtengo las dos primeras letras de la banda de musica
          child: Text(band.name.substring(0, 2)),
          backgroundColor: Colors.blue[100],
        ),
        title: Text(band.name),
        trailing: Text('${band.votes}', style: TextStyle(fontSize: 20)),
        onTap: () {
          //print(band.id);
          //Emito un evento para ser escuchado en el server
          socketService.emit('vote-band', {'id': band.id});
        },
      ),
    );
  }

  addNewBand() {
    final textController = new TextEditingController();

    if (Platform.isAndroid) {
      //Dialogo solo para android
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('New Band name'),
              content: TextField(
                controller: textController,
              ),
              actions: <Widget>[
                MaterialButton(
                    child: Text('Add'),
                    elevation: 5,
                    color: Colors.blue,
                    onPressed: () => addBandCustomToList(textController.text))
              ],
            );
          });
    }
    showCupertinoDialog(
        context: context,
        builder: (_) {
          return CupertinoAlertDialog(
            title: Text('New Band name'),
            content: CupertinoTextField(
              controller: textController,
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                isDefaultAction: true,
                child: Text('Add'),
                onPressed: () => addBandCustomToList(textController.text),
              ),
              CupertinoDialogAction(
                  isDestructiveAction: true,
                  child: Text('Dismiss'),
                  //para cerrar el dialogo en el onPress seria...
                  onPressed: () => Navigator.pop(context))
            ],
          );
        });
  }

//Metodo que agrega bandas a la lista
  void addBandCustomToList(String name) {
    final socketService = Provider.of<SocketService>(context, listen: false);

    print(name);
    if (name.length > 1) {
      //podemos agregar
      // this.bands.add(new Band(id: DateTime.now().toString(), name: name, votes: 3));
      //Como es un statefullWidget llamamos al setState para refrescar
      //setState(() {});

      //Lo anterior ya no es necesario,pues simplemente necesito enviar una comunicacion
      //al server de sockets , mandarle la informacion respectiva y el server de sockets
      //al agregarlo y crear uno nuevo deberia de notificarme con esa una banda , y como tengo
      //el listener respectivo deberia de refrescarse todo de manera automatica

      //emitir: add-band
      //el valor seria {name:'name'}
      socketService.emit('add-band', {'name': name});
    }
    Navigator.pop(context);
  }

//Muestra la grafica
  Widget _showGraph() {
    Map<String, double> dataMap = new Map();
    // "Flutter": 5,
    // "React": 3,
    // "Xamarin": 2,
    // "Ionic": 2,

    bands.forEach((band) {
      dataMap.putIfAbsent(band.name, () => band.votes.toDouble());
    });

    final List<Color> colorList = [
      Colors.blue[50],
      Colors.blue[200],
      Colors.pink[50],
      Colors.pink[200],
      Colors.yellow[50],
      Colors.yellow[200],
    ];
    //final _screenSize  = MediaQuery.of(context).size;

    return Container(
    padding: EdgeInsets.symmetric(horizontal:10),
        width: double.infinity,
        height: 250.0,
        child: PieChart(
          dataMap: dataMap,
          animationDuration: Duration(milliseconds: 800),
          //chartLegendSpacing: 32,
          chartRadius: MediaQuery.of(context).size.width / 2.2,
          colorList: colorList,
          initialAngleInDegree: 8,
          chartType: ChartType.ring,
          ringStrokeWidth: 17,
          centerText: "Bands",
          legendOptions: LegendOptions(
            showLegendsInRow: false,
            legendPosition: LegendPosition.right,
            showLegends: true,
            legendShape: BoxShape.circle,
            legendTextStyle: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          chartValuesOptions: ChartValuesOptions(
            showChartValueBackground: true,
            showChartValues: true,
            showChartValuesInPercentage: false,
            showChartValuesOutside: false,
          ),
        ));
  }
}
